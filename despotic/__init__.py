__version__ = '1.1.0'
__all__ = ["cloud", "collPartner", "composition", "despoticError", \
           "dustProp", "emitter", "emitterData", "fetchLamda", \
           "lineProfLTE", "radiation", "refreshLamda"]

# Import the classes and functions from the files of the same
# name. This allows users to access the classes and functions
# directly, as despotic.cloud, rather than having to use the
# cumbersome notation despotic.cloud.cloud or similar.
from cloud import cloud
try:
    from cloud import run_cloud_gui
except:
    pass
from collPartner import collPartner
from composition import composition
from dustProp import dustProp
from emitter import emitter
from emitterData import emitterData
from fetchLamda import fetchLamda
from lineProfLTE import lineProfLTE
from radiation import radiation
from refreshLamda import refreshLamda
import chemistry
